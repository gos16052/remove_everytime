import os
import codecs
from selenium import webdriver
from selenium.webdriver.common.alert import Alert
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.common.exceptions import TimeoutException
from time import sleep
import platform
import wget
import zipfile
import argparse

everytime_login = "https://everytime.kr/login"
everytime_main = "https://everytime.kr/"
everytime_posts = "https://everytime.kr/myarticle/p/"
everytime_comments = "https://everytime.kr/mycommentarticle/p/"

def download_chrome_webdriver():
    OS = platform.system()
    if OS == 'Windows':
        executable = "chromedriver.exe"
        filename = "chromedriver_win32.zip"
    elif OS == 'Linux':
        executable = "chromedriver"
        filename = "chromedriver_linux64.zip"
    elif OS == 'Darwin': # mac
        executable = "chromedriver"
        filename = "chromedriver_mac64.zip"
    if not os.path.exists(executable):
        print("download chrome driver ...", flush=True)
        wget.download("https://chromedriver.storage.googleapis.com/2.44/" + filename)
        print("unzip %s" % (filename), flush=True)
        driver_zip = zipfile.ZipFile(filename)
        driver_zip.extractall(".")
        driver_zip.close()
        os.remove(filename)
    return executable

def get_articles(driver, link):
    hrefs = []
    pageno = 1

    pagelink = link + "%d" % (pageno)
    driver.get(pagelink)
    articles = driver.find_elements_by_class_name("articles")[0]
    articles = articles.find_elements_by_class_name("article")
    while len(articles) > 0:
        for article in articles:
            hrefs.append(article.get_attribute("href"))
        pageno += 1
        pagelink = link + "%d" % (pageno)
        driver.get(pagelink)
        articles = driver.find_elements_by_class_name("articles")[0]
        articles = articles.find_elements_by_class_name("article")
    return hrefs

def remove_posts(driver, f, is_remove=False):
    print("remove posts", flush=True)
    f.write(u"remove posts\n")
    f.write(u"----------------\n")
    hrefs = get_articles(driver, everytime_posts)
    for href in hrefs:
        print(href, flush=True)
        driver.get(href)
        articles = driver.find_elements_by_class_name("articles")[0]
        article = articles.find_elements_by_class_name("article")[0]
        status = article.find_elements_by_class_name("status")[0]
        contents = article.find_elements_by_class_name("large")
        for content in contents:
            f.write(content.get_attribute('innerHTML'))
            f.write(u'\n')
        f.write(u"----------------\n")
        if is_remove:
            delbutton = status.find_elements_by_class_name("del")[0]
            delbutton.click()
            Alert(driver).accept()

def remove_comments(driver, f, is_remove=False):
    print("remove comments", flush=True)
    f.write(u"remove comments\n")
    f.write(u"----------------\n")
    hrefs = get_articles(driver, everytime_comments)
    for href in hrefs:
        print(href, flush=True)
        driver.get(href)
        try:
            myElem = WebDriverWait(driver, 1).until(EC.presence_of_element_located((By.CLASS_NAME, 'comments')))
        except TimeoutException:
            print("Loading took too much time!")
            continue
        articles = driver.find_elements_by_class_name("articles")[0]
        comments = articles.find_elements_by_class_name("comments")[0]
        delbuttons = comments.find_elements_by_class_name("del")
        for delbutton in delbuttons:
            pp = delbutton.find_element_by_xpath("..")
            pp = pp.find_element_by_xpath("..")
            content = pp.find_elements_by_class_name("large")[0]
            f.write(content.get_attribute('innerHTML'))
            f.write(u'\n----------------\n')
            if is_remove:
                while True:
                    try:
                        delbutton.click()
                        Alert(driver).accept()
                        break
                    except:
                        print("no page loaded.", flush=True)
                        sleep(1)
                        # to close abuseForm
                        try:
                            driver.find_element_by_id("abuseForm").find_elements_by_class_name("close")[0].click()
                        except:
                            sleep(1)



def main():
    parser = argparse.ArgumentParser()
    parser.add_argument('--remove', '-r', dest='remove',
            action='store_true',
            help="remove posts and comments")
    args = parser.parse_args()

    is_remove = args.remove
    if is_remove:
        print("remove posts and comments", flush=True)
    else:
        print("logging mode: your posts and comments are loggged to log.txt", flush=True)

    executable = download_chrome_webdriver()
    chrome = os.path.join(os.getcwd(), executable)

    f = codecs.open("log.txt", "a+", "utf-8")
    driver = webdriver.Chrome(chrome)
    driver.implicitly_wait(2)

    driver.get("https://everytime.kr/login")
    while True:
        print("please login ...", flush=True)
        sleep(1)
        if driver.current_url == everytime_main:
            break

    remove_posts(driver, f, is_remove)
    remove_comments(driver, f, is_remove)
    f.close()

if __name__ == "__main__":
    main()
